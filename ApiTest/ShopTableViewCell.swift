//
//  ShopTableViewCell.swift
//  ApiTest
//
//  Created by 扶紀沅 on 2020/8/8.
//  Copyright © 2020 扶紀沅. All rights reserved.
//

import UIKit

class ShopTableViewCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var priceLb: UILabel!
    
    var model: ShopModel? {
        didSet {
            title.text = model?.shop
            priceLb.text = "價錢: \(model!.price)"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
