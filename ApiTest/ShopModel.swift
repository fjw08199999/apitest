//
//  ShopModel.swift
//  ApiTest
//
//  Created by 扶紀沅 on 2020/8/8.
//  Copyright © 2020 扶紀沅. All rights reserved.
//

import Foundation

struct ShopModel: Codable {
    let shop: String
    let price: Int
}
