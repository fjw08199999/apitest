//
//  ViewController.swift
//  ApiTest
//
//  Created by 扶紀沅 on 2020/8/8.
//  Copyright © 2020 扶紀沅. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let tableView: UITableView = UITableView(frame: .zero, style: .plain)
    var model: [ShopModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
        
        let nib = UINib(nibName: "ShopTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ShopTableViewCell")
        tableView.dataSource = self
        
        
        let dataTask = URLSession.shared.dataTask(with: URL(string: "http://127.0.0.1:5000/")!) { (data, uRLResponse, error) in
            
            if let error = error {
                print("error = \(error)")
                return
            }
            guard let data = data else {
                print("get data error")
                return
            }
            let pythonEcho = String(data: data, encoding: .utf8)
            print("sss = \(pythonEcho)")
            guard let model = try? JSONDecoder().decode([ShopModel].self, from: data) else {
                print("decoder error")
                return
            }
            self.model = model
            DispatchQueue.main.async {
                self.tableView.reloadSections([.zero], with: .automatic)
            }
        }
        dataTask.resume()
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShopTableViewCell", for: indexPath) as! ShopTableViewCell
        cell.model = model[indexPath.row]
        return cell
    }
    
    
}
